﻿using loan.Models;
using QSI.Persistence.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace loan.Repositories
{
    public interface ICityDao : BaseDao<City, long>
    {
    }
}
