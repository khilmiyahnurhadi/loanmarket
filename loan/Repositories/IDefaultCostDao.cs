﻿using loan.Models.Calculators;
using QSI.Persistence.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace loan.Repositories
{
    public interface IDefaultCostDao : BaseDao<DefaultCost,long>
    {
    }
}
