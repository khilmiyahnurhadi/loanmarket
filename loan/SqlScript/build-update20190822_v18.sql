
    alter table t_city 
        add constraint FKCD56F6F29F43C5E8 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FKF4D4A3C366EA4401 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FK2ED89023F77EA590 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK68F26EA679E440E0 
        foreign key (PostalCodeId_id) 
        references t_postalcode
    alter table t_village 
        add constraint FK68F26EA656FBB2C 
        foreign key (SubDistrict_id) 
        references t_subdistrict