
    create table t_ptkp (
        Id  bigserial,
       Status boolean not null,
       MateHasIncome boolean not null,
       TotalPTKP int8 not null,
       primary key (Id)
    )
    alter table t_city 
        add constraint FK3A4FEA4B6CBF6E1A 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK4CC74CA17B9DDD05 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FKB53B1A817F3506F 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK531F23C9D5A9145 
        foreign key (PostalCodeId_id) 
        references t_postalcode