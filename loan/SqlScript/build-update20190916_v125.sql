
    alter table t_city 
        add constraint FKBA645FAE9B342D64 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK3EAF36E8F7FC2C1D 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FK6AD86749A8038C9F 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK89CDFE57526570DC 
        foreign key (PostalCodeId_id) 
        references t_postalcode