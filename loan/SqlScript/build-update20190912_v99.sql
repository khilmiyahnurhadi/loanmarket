
    alter table t_city 
        add constraint FKC911E41166DF547 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK46B5154DA6C036C5 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FK86140FAC866540E1 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FKB6D8D55AC7A9701D 
        foreign key (PostalCodeId_id) 
        references t_postalcode