
    alter table t_city 
        add constraint FKEDA416ADD9FFFA2E 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK2804056E102FC075 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FKE2B5D5306CDE543A 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK17FD67ECB83E25F8 
        foreign key (PostalCodeId_id) 
        references t_postalcode