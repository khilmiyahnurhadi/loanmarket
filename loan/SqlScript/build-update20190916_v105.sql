
    alter table t_city 
        add constraint FK5A213169424FAABD 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FKD79EEBD96D0F575 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FKB6FB5EE7D53B9F72 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK51E90252122311A 
        foreign key (PostalCodeId_id) 
        references t_postalcode