
    alter table t_city 
        add constraint FK581D0DAA38779F12 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK5AE5D82112B20791 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FK2916D7DC4F33C191 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK1F04B382C0A910 
        foreign key (PostalCodeId_id) 
        references t_postalcode