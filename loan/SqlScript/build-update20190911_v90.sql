
    alter table t_city 
        add constraint FKAC0EF227F22F297 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK62DC6AA793EAD235 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FKA40FF57112A77564 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK435D477196534D47 
        foreign key (PostalCodeId_id) 
        references t_postalcode