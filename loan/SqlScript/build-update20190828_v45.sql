
    alter table t_city 
        add constraint FKE0336B94B42D5E5D 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK78C1C09E5883F8C1 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FK17C24685D37AF39B 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FKF4AB6766900B0937 
        foreign key (PostalCodeId_id) 
        references t_postalcode