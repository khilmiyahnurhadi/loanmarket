
    alter table t_city 
        add constraint FK2689B480EC334851 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK83558B8C466355B9 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FK327781D8367777B6 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK5C948F5DBFB89117 
        foreign key (PostalCodeId_id) 
        references t_postalcode