
    alter table t_city 
        add constraint FK5641A10378BBB0AC 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK3B46FBDF7BF2D0D3 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FKA239BF415B572444 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK8D63A80186E8ED8 
        foreign key (PostalCodeId_id) 
        references t_postalcode