
    alter table t_city 
        add constraint FK693D491A182C2D3B 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FKDB8EA8C0E8D4D607 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FK9D39206AC37C8367 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FKF37FE2F7B8CF0ADB 
        foreign key (PostalCodeId_id) 
        references t_postalcode