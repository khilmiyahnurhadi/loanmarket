
    alter table t_city 
        add constraint FKFB8032C7987DDB58 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FKF88A9C8DBC97D798 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FK449800FA24640815 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FKD2AED0DED5EC6264 
        foreign key (PostalCodeId_id) 
        references t_postalcode