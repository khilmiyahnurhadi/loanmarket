
    alter table t_city 
        add constraint FKA6630955A998D77F 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK813D39A1E6403CC2 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FKF92DAB1C1824FA60 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK540DAF9DF9D554E3 
        foreign key (PostalCodeId_id) 
        references t_postalcode