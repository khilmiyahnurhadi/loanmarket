
    alter table t_city 
        add constraint FKF3CF232CF8252040 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FKE748C0C158C25F2B 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FK5495221777328B16 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FKF3CD20E13E53A2EC 
        foreign key (PostalCodeId_id) 
        references t_postalcode