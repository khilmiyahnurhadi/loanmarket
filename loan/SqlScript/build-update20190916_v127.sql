
    alter table t_city 
        add constraint FK48EA544D77DF819B 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK36462CDBE3BD3866 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FK8423E56E606BCDB8 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK7D40FC42E4E70389 
        foreign key (PostalCodeId_id) 
        references t_postalcode