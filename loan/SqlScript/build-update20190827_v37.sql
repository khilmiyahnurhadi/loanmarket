
    alter table t_city 
        add constraint FK56754325A940F2EC 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FKAEF24D69E1AD6C1A 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FK32970D176D7DC7EA 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK1F075C38A89AF5B 
        foreign key (PostalCodeId_id) 
        references t_postalcode