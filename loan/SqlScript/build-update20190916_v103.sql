
    alter table t_city 
        add constraint FKE6B36F932E2F729D 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FKD5041F6EEC05F700 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FKB803E5B159365D1 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FKC63DBA938DE4D882 
        foreign key (PostalCodeId_id) 
        references t_postalcode