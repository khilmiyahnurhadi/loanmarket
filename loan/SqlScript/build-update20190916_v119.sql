
    alter table t_city 
        add constraint FK7F950758B5692FAA 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK8ADC0CAFAB1DCB58 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FKAAF4B18EC70EF0F6 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FKD4C7D3D9DD44E8A1 
        foreign key (PostalCodeId_id) 
        references t_postalcode