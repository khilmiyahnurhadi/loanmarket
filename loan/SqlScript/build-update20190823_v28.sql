
    alter table t_city 
        add constraint FK6ED248CE83DB1203 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FKAC5B5162A18A40D5 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FK412D65FA5861CA24 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK46A7F544FB12B185 
        foreign key (PostalCodeId_id) 
        references t_postalcode