
    alter table t_city 
        add constraint FK4F8675ACA056E571 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK5614B6A0878BB61F 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FKE4D9CC865586373C 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK6F8FADBF5EF3C353 
        foreign key (PostalCodeId_id) 
        references t_postalcode
    alter table t_village 
        add constraint FK6F8FADBF79C22A3D 
        foreign key (SubDistrict_id) 
        references t_subdistrict