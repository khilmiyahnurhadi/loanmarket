
    alter table t_city 
        add constraint FK9996CA9CDD3DB75 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FKDBF0DBF1D5B7027 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FK2233D7BA49E75606 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK6B889590261F9872 
        foreign key (PostalCodeId_id) 
        references t_postalcode