
    alter table t_city 
        add constraint FK7EAF3CEBD1EC98CC 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FKF172AF50EA8D1C26 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FKC357822D51080BA0 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FKB5E7B2497554C389 
        foreign key (PostalCodeId_id) 
        references t_postalcode