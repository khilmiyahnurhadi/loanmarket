
    alter table t_city 
        add constraint FK3D1A82CFE4A110D5 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK2B65C46692B97CAE 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FK118BB67B70A873CC 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK76E2C794BCF78DF 
        foreign key (PostalCodeId_id) 
        references t_postalcode