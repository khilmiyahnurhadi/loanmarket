
    create table t_city (
        Id  bigserial,
       CityName varchar(255) not null,
       ProvinceId_id int8 not null,
       primary key (Id)
    )
    create table t_postalcode (
        Id  bigserial,
       PostalCodeNumber varchar(255) not null,
       SubDistrictId_id int8 not null,
       primary key (Id)
    )
    create table t_province (
        Id  bigserial,
       ProvinceName varchar(255) not null,
       primary key (Id)
    )
    create table t_subdistrict (
        Id  bigserial,
       SubDistrictName varchar(255) not null,
       CityId_id int8 not null,
       primary key (Id)
    )
    create table t_village (
        Id  bigserial,
       VillageName varchar(255) not null,
       PostalCodeId_id int8 not null,
       SubDistrict_id int8,
       primary key (Id)
    )
    alter table t_city 
        add constraint FKBA764AA9BB83059E 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK68CAAB2459789521 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FKE72A4DCBDDAE0E7 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK21AFA393B99940F 
        foreign key (PostalCodeId_id) 
        references t_postalcode
    alter table t_village 
        add constraint FK21AFA3934B899E5 
        foreign key (SubDistrict_id) 
        references t_subdistrict