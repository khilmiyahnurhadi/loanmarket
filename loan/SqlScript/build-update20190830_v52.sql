
    alter table t_city 
        add constraint FKB3E7368074B887AD 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK4AB6F4E1B39C714A 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FKD0208E2EDE8945F5 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK34E66E08416390D9 
        foreign key (PostalCodeId_id) 
        references t_postalcode