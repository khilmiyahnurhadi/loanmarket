
    alter table t_city 
        add constraint FKCC954AAC7DC9AF09 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK328C7D2F6EB3F3C 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FKB6A5BF1D97645581 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK93038D886CEDCB58 
        foreign key (PostalCodeId_id) 
        references t_postalcode