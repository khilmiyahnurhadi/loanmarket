
    alter table t_city 
        add constraint FK10A4C8D79C74C720 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK6EC640768DF6EF11 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FK9F573CA2E63A4BB8 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK27563288437EC92 
        foreign key (PostalCodeId_id) 
        references t_postalcode