
    alter table t_city 
        add constraint FK75E9FBB177282FA2 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK589E56D0F7F4F7A5 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FK3452DA8DB2DA8F0F 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK56CE3D8C1614B28D 
        foreign key (PostalCodeId_id) 
        references t_postalcode