
    alter table t_city 
        add constraint FK58876FF81FFD71A1 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK8C0251DE62276542 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FK13C04D6133F4DCC 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FKFE1641294FB459F9 
        foreign key (PostalCodeId_id) 
        references t_postalcode