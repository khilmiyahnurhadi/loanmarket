
    alter table t_city 
        add constraint FK6191B4F1E9BBB2BC 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FKD50474178151F52 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FK1977A3ECED5533DA 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FKF589404C1440F9F9 
        foreign key (PostalCodeId_id) 
        references t_postalcode