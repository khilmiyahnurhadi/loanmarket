
    create table t_simulation (
        Id  bigserial,
       PropertyPrices int8 not null,
       PTKP int8 not null,
       DownPayment int8 not null,
       LoanInterestCost int8 not null,
       HomeInsuranceCost int8 not null,
       LifeInsuranceCost int8 not null,
       AdministrativeCost int8 not null,
       BPHTB int8 not null,
       NotaryFee int8 not null,
       PNPB int8 not null,
       StampDuty int8 not null,
       primary key (Id)
    )
    alter table t_city 
        add constraint FK6CD5E84A91845961 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK776533A183F67162 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FK4CFEEA1F25451386 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FKDF3EACFAD96EFF5B 
        foreign key (PostalCodeId_id) 
        references t_postalcode