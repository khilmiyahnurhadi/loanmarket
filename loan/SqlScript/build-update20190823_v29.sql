
    alter table t_city 
        add constraint FK3C60651DCDBBF06C 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FKB96EF8469429B1D 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FKF7657DD1A7B98AEB 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FKA8D7220216948FFB 
        foreign key (PostalCodeId_id) 
        references t_postalcode