
    alter table t_city 
        add constraint FK2AF856B4772221F3 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FKD8CF5366380CB50 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FK4B8241268031B0F0 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK61171EDF68C767B6 
        foreign key (PostalCodeId_id) 
        references t_postalcode