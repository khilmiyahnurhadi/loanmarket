
    alter table t_city 
        add constraint FKC386AF7A247A994 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK3F863FC692637F72 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FK854F76EC4B6D4378 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK29FBD1BA986E279 
        foreign key (PostalCodeId_id) 
        references t_postalcode