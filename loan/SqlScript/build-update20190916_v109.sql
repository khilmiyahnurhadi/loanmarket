
    alter table t_city 
        add constraint FK59F836A4D5CBB44 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK4C9F6410C654E1D3 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FKD10DBB142E6706A8 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK9EEB0C0F16E80925 
        foreign key (PostalCodeId_id) 
        references t_postalcode