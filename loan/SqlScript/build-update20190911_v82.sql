
    alter table t_city 
        add constraint FK2691CCC734B65176 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK350E942A6B53FD0C 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FK406E386B6631625F 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK50F315C322F4BEE3 
        foreign key (PostalCodeId_id) 
        references t_postalcode