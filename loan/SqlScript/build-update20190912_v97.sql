
    alter table t_city 
        add constraint FKE7E56C19A66EE61F 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK210C3C4C4B7332CC 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FKB4A32F177589B6ED 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK1F3B0F8EC7C65BA2 
        foreign key (PostalCodeId_id) 
        references t_postalcode