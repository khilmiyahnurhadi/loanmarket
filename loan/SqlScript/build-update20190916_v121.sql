
    alter table t_city 
        add constraint FKCC2C6D68DFC38E43 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FKE78675F9C03AEF70 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FKA3440DFFAD9CB49E 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK8666C40BD2AB8C93 
        foreign key (PostalCodeId_id) 
        references t_postalcode