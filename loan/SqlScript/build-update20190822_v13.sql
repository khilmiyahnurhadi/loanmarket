
    alter table t_city 
        add constraint FKA6F2C32B8BE7B0B7 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK5AB74FABB68FF26D 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FK9654A4E8D3B0BB75 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FKD361587A5646F386 
        foreign key (PostalCodeId_id) 
        references t_postalcode
    alter table t_village 
        add constraint FKD361587AA587D70B 
        foreign key (SubDistrict_id) 
        references t_subdistrict