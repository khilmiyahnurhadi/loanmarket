
    create table t_city (
        Id  bigserial,
       CityName varchar(255) not null,
       ProvinceId_id int8 not null,
       primary key (Id)
    )
    create table t_postalcode (
        Id  bigserial,
       PostalCodeNumber varchar(255) not null,
       SubDistrictId_id int8 not null,
       primary key (Id)
    )
    create table t_province (
        Id  bigserial,
       ProvinceName varchar(255) not null,
       primary key (Id)
    )
    create table t_subdistrict (
        Id  bigserial,
       SubDistrictName varchar(255) not null,
       CityId_id int8 not null,
       primary key (Id)
    )
    create table t_village (
        Id  bigserial,
       VillageName varchar(255) not null,
       PostalCodeId_id int8 not null,
       primary key (Id)
    )
    alter table t_city 
        add constraint FK7D5B475FAC2A612C 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FKD14E52443063663E 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FKE5DF8B7D7938D796 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FKF2306464D38AB96B 
        foreign key (PostalCodeId_id) 
        references t_postalcode