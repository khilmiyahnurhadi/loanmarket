
    alter table t_city 
        add constraint FKCBC8086945CE0795 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK8613A53376134C63 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FK1F95DE75A7D60E6C 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FKB7C4821F6B616B6C 
        foreign key (PostalCodeId_id) 
        references t_postalcode