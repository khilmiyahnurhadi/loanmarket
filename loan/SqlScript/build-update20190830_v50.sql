
    alter table t_city 
        add constraint FK9E6B81602E7BACEF 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK773D817C5BA59E35 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FKCD0F620A32DD7BFF 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FKD9992330E752010C 
        foreign key (PostalCodeId_id) 
        references t_postalcode