
    alter table t_city 
        add constraint FKF4585C80C5A9C0AB 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK621D25CCB20BD6BE 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FKDF81E323A242898C 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK26F9F01324F20AD8 
        foreign key (PostalCodeId_id) 
        references t_postalcode