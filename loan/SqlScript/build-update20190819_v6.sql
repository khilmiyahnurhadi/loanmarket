
    create table t_city (
        Id  bigserial,
       CityName varchar(255) not null,
       primary key (Id)
    )
    create table t_location (
        Id  bigserial,
       Postalcode varchar(255) not null,
       SubDistrict varchar(255) not null,
       CityId_id int8 not null,
       ProvinceId_id int8 not null,
       primary key (Id)
    )
    create table t_province (
        Id  bigserial,
       ProvinceName varchar(255) not null,
       primary key (Id)
    )
    alter table t_location 
        add constraint FK1BF3FE60A9810C0A 
        foreign key (CityId_id) 
        references t_city
    alter table t_location 
        add constraint FK1BF3FE6034696378 
        foreign key (ProvinceId_id) 
        references t_province