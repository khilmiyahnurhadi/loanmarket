
    alter table t_city 
        add constraint FK3599071613DBCE51 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK57EF76172BEA03A 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FKBBC8758F4A210124 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK98B30BC0E10A831 
        foreign key (PostalCodeId_id) 
        references t_postalcode