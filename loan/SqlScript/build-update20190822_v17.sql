
    alter table t_city 
        add constraint FK95222DCC1B408CDF 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FKB13600188E731FCA 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FK24A06EBFF2E9078B 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK8C4E512FF3051FE1 
        foreign key (PostalCodeId_id) 
        references t_postalcode
    alter table t_village 
        add constraint FK8C4E512F1830B577 
        foreign key (SubDistrict_id) 
        references t_subdistrict