
    alter table t_city 
        add constraint FK17B9592053058B84 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK9CBC7C64BCE6D0A9 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FK9881EAE77DB3BCE1 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FKF3962E728AEF9B9C 
        foreign key (PostalCodeId_id) 
        references t_postalcode