
    alter table t_city 
        add constraint FK34BFA676DC3A71D 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK48CBB053CCA6023E 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FKC14AB4C167E56C38 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK5B81619D9A5B1145 
        foreign key (PostalCodeId_id) 
        references t_postalcode
    alter table t_village 
        add constraint FK5B81619D86D1C76E 
        foreign key (SubDistrict_id) 
        references t_subdistrict