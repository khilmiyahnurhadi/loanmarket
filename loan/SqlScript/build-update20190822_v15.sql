
    alter table t_city 
        add constraint FK23CD7BC6776212DB 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK38E6543867636FCB 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FK6A82DF09310C62B 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK419E9C4D27F250CB 
        foreign key (PostalCodeId_id) 
        references t_postalcode
    alter table t_village 
        add constraint FK419E9C4DA79E0256 
        foreign key (SubDistrict_id) 
        references t_subdistrict