
    alter table t_city 
        add constraint FK9FDC1F064907567F 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK628563CB38934290 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FK252D7AD45882D805 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FKC110BE2BA861E66D 
        foreign key (PostalCodeId_id) 
        references t_postalcode