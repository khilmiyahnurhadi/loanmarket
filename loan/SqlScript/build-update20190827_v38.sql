
    alter table t_city 
        add constraint FK57094C74B391DD9B 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK776CC1C84CA2B8FC 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FKE707881BFE765CD0 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK5BA689097EEE017F 
        foreign key (PostalCodeId_id) 
        references t_postalcode