
    alter table t_city 
        add constraint FK1D5F0E5FFF3A77FF 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK364F97FE33096911 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FKF13168B6110D6A5B 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK9BC444291B8EB8AD 
        foreign key (PostalCodeId_id) 
        references t_postalcode