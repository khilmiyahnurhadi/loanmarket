
    alter table t_city 
        add constraint FK3B7AC9387C220FB3 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK951B6CDEBA6F6801 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FKE5A2FDFD74CF354F 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FKA5780D6B8BA377B 
        foreign key (PostalCodeId_id) 
        references t_postalcode