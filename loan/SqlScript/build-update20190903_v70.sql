
    alter table t_city 
        add constraint FKA2B5AAC72655AD18 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK210C45B1F089FF02 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FKA9319569F43145D6 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK64B8836E4757EC7E 
        foreign key (PostalCodeId_id) 
        references t_postalcode