
    alter table t_city 
        add constraint FK593FA2CA5A7D4191 
        foreign key (ProvinceId_id) 
        references t_province
    alter table t_postalcode 
        add constraint FK38FE1EAC44DA9904 
        foreign key (SubDistrictId_id) 
        references t_subdistrict
    alter table t_subdistrict 
        add constraint FK5DBE0F1D9DE65730 
        foreign key (CityId_id) 
        references t_city
    alter table t_village 
        add constraint FK4ABE649A1952BE33 
        foreign key (PostalCodeId_id) 
        references t_postalcode