﻿using loan.Commands;
using loan.Dtos.Calculators;
using loan.Models.Calculators;
using loan.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace loan.Services.Impl
{
    public class CalculatorService : ICalculatorService
    {
        public readonly IPTKPDao pTKPDao;
        public readonly IDefaultCostDao defaultCostDao;
        public readonly IPercentDao percentDao;
        public readonly ISimulationDao simulationDao;

        public CalculatorService(IPTKPDao pTKPDao, IDefaultCostDao defaultCostDao, IPercentDao percentDao, ISimulationDao simulationDao)
        {
            this.pTKPDao = pTKPDao;
            this.defaultCostDao = defaultCostDao;
            this.percentDao = percentDao;
            this.simulationDao = simulationDao;
        }

        public IList<PTKPDto> GetAllPTKP()
        {
            var PTKPs = this.pTKPDao.GetAll();
            IList<PTKPDto> PTKPDtos = new List<PTKPDto>();
            foreach (var ptkp in PTKPs)
            {
                PTKPDtos.Add(new PTKPDto(ptkp.Status, ptkp.MateHasIncome, ptkp.Burden, ptkp.TotalPTKP));
            }

            return PTKPDtos;
        }

        public void SimulationResult(SimulationCommand command)
        {
            Simulation NewSimulation = new Simulation();
            NewSimulation.PropertyPrices = command.PropertyPrices;
            NewSimulation.PTKP = command.PTKP;
            NewSimulation.DownPayment = command.DownPayment;
            var LoanInterestPercent = this.percentDao.Get(1);
            NewSimulation.LoanInterestCost = (LoanInterestPercent.PercentNumber / 100) * command.PropertyPrices;
            var HomeInsurancePercent = this.percentDao.Get(2);
            NewSimulation.HomeInsuranceCost = (HomeInsurancePercent.PercentNumber / 100) * command.PropertyPrices;
            var LifeInsurancePrice = this.defaultCostDao.Get(1);
            NewSimulation.LifeInsuranceCost = LifeInsurancePrice.Cost;
            var AdministrativePercent = this.percentDao.Get(3);
            NewSimulation.AdministrativeCost = (AdministrativePercent.PercentNumber / 100) * command.PropertyPrices;
            var BPHTBPercent = this.percentDao.Get(4);
            NewSimulation.BPHTB = (BPHTBPercent.PercentNumber / 100) * (command.PropertyPrices - command.PTKP);
            if (command.PropertyPrices<=500000000)
            {
                NewSimulation.NotaryFee = 7000000;
            }
            else
            {
                NewSimulation.NotaryFee = 10000000;
            }
            var PNPBPrice = this.defaultCostDao.Get(2);
            NewSimulation.PNPB = PNPBPrice.Cost;
            var StampPrice = this.defaultCostDao.Get(3);
            NewSimulation.StampDuty = StampPrice.Cost;
            this.simulationDao.Save(NewSimulation);
        }

        public IList<SimulationDto> GetAllSimulation()
        {
            var Simulations = this.simulationDao.GetAll();
            IList<SimulationDto> SimulationDtos = new List<SimulationDto>();
            foreach (var simulation in Simulations)
            {
                SimulationDtos.Add(new SimulationDto(simulation.Id, simulation.PropertyPrices, simulation.PTKP, simulation.DownPayment, simulation.LoanInterestCost, simulation.HomeInsuranceCost, simulation.LifeInsuranceCost, simulation.AdministrativeCost, simulation.BPHTB, simulation.NotaryFee, simulation.PNPB, simulation.StampDuty));
            }

            return SimulationDtos;
        }


       public void EditHomeInsuranceCost(EditHomeInsuranceCommand command)
        {
            var Simulation = this.simulationDao.Get(command.Id);
            Simulation.HomeInsuranceCost = command.NewCost;
            this.simulationDao.Save(Simulation);
        }
       public void EditLifeInsuranceCost(EditLifeInsuranceCommand command)
        {
            var Simulation = this.simulationDao.Get(command.Id);
            Simulation.LifeInsuranceCost = command.NewCost;
            this.simulationDao.Save(Simulation);
        }
    }
}
