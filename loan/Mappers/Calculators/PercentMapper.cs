﻿using FluentNHibernate.Mapping;
using loan.Models.Calculators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace loan.Mappers.Calculators
{
    public class PercentMapper : ClassMap<Percent>
    {
        public PercentMapper()
        {
            Table("t_percent");
            Id(x => x.Id).GeneratedBy.Native();
            Map(x => x.CostName).Not.Nullable();
            Map(x => x.PercentNumber).Not.Nullable();
        }
    }
}
