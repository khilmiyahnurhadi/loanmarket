﻿using FluentNHibernate.Mapping;
using loan.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace loan.Mappers
{
    public class PostalCodeMapper : ClassMap< PostalCode>
    {
        public PostalCodeMapper()
        {
            Table("t_postalcode");
            Id(x => x.Id).GeneratedBy.Native();
            Map(x => x.PostalCodeNumber).Not.Nullable();
            References<SubDistrict>(x => x.SubDistrictId).Not.Nullable();
            HasMany(x => x.Villages)
                .Inverse()
                .AsBag()
                .LazyLoad();
        }
    }
}
