﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace loan.Dtos
{
    public class ProvinceDto
    {
        public long Id { set; get; }
        public string ProvinceName { set; get; }
        

        public ProvinceDto()
        {

        }

        public ProvinceDto(long id, string provinceName)
        {
            Id = id;
            ProvinceName = provinceName;
        }
    }
}
