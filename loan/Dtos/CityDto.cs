﻿using loan.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace loan.Dtos
{
    public class CityDto
    {

        public long Id { set; get; }
        public string CityName { set; get; }
        public long ProvinceId { set; get; }

        public CityDto()
        {

        }
        public CityDto(long id, string cityName, long provinceId)
        {
            Id = id;
            CityName = cityName;
            ProvinceId = provinceId;
        }
    }
}
