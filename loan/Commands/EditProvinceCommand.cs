﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace loan.Commands
{
    public class EditProvinceCommand
    {
        public long IdProvince { set; get; }
        public string ProvinceName { set; get; }
    }
}
