﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace loan.Commands
{
    public class SimulationCommand
    {
        public long PropertyPrices { set; get; }
        public long PTKP { set; get; }
        public long DownPayment { set; get; }
    }
}
