﻿using QSI.Persistence.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace loan.Models
{
    public class Village : Entity<long>
    {
        public virtual string VillageName { set; get; }
        public virtual PostalCode PostalCodeId { set; get; }

        public override bool Equals(object obj)
        {
            var village = obj as Village;
            return village != null &&
                   VillageName == village.VillageName &&
                   EqualityComparer<PostalCode>.Default.Equals(PostalCodeId, village.PostalCodeId);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(VillageName, PostalCodeId);
        }
    }
}
